snapnbackup is a collection of scripts to aid keeping a TimeMachine like backup
with FreeNAS and UFS.

== Features ==

* Create snapshots.
* Backs up snapshots to external media.
* Removes snapshots before external media fills up.
* Removes snapshots before mounted snapshots hits UFS limit.


== Installation ==

1. Copy all files in the same folder in FreeNAS somewhere.
2. Configure snap.config, adjust mainly the installation location.
3. Add a cron job in FreeNAS to call snapnbackup.sh daily or weekly.

