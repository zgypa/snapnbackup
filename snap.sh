#!/bin/sh
#
#	$Id: snap.sh,v 1.7 2009/04/04 07:51:23 cvs Exp $
#
#  This will create a snapshot and mount it.
#

CONFIG="$(dirname ${0})/snap.config"
if [ -f ${CONFIG} ]; then
        . ${CONFIG}
else
        echo "FATAL: cannot find config file ${CONFIG}"
        exit
fi

if test ! -d "${1}"
then
	echo "Usage: ${0##*/} /path" >&2
	exit 64
fi
#
base="${1}"
#

if test -f "${base}/.snap/${date}"
then
	echo "snapshot ${base}/.snap/${date}: file exists." >&2
	exit 73
fi
#
# create snapshot
if ! mount -u -o snapshot "${base}/.snap/${date}" "${base}"
then
	exit 73
fi
#
# look for a free device
for nr in `jot 40`
do
	if test ! -c "/dev/md${nr}"
	then
		#
		# found a free device
		break
	fi
done
#
# detach old device info (paranoid)
# mdconfig -d -u "${nr}" 2>> /dev/null
#
# create device
mdconfig -a -t vnode -o readonly -f "${base}/.snap/${date}" -u "${nr}"
#
# create a directory and mount the backup
mkdir -p "${base}/${SNAPSHOTSDIR}/${date}"
mount -r -o noexec,nosuid "/dev/md${nr}" "${base}/${SNAPSHOTSDIR}/${date}"
#
# eof

