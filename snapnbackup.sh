#!/bin/bash
#
# Take a snapshot, mount it, and back it up to external media.
#
#

set -e

debug () {
	if [ "$DEBUG" -eq 1 ]; then
		echo DEBUG: "$*"
	fi
}

log () {
	echo LOG: "$*"
}


fatal () {
	echo FATAL: "$*" 1>&2
}

# Location of config file 
CONFIG="$(dirname ${0})/snap.config"

if [ -f ${CONFIG} ]; then
        . ${CONFIG}
else
        fatal "cannot find config file ${CONFIG}"
        exit
fi

usage () {
	# Get percantage of used space on drive
	USAGE=$(df -h | grep ${1} | awk '{ print $5 };')
	if [ -z "${USAGE}" ]; then
		echo -1;
	fi
	
	# Strip off the % sign
	echo ${USAGE%\%}
}

toomanysnaps () {
	log "Too many mounted snapshots: $(numberOfSnapShots) > ${MAXNUMBEROFSNAPSHOTS}. Detaching oldest snapshot."
}

# echo "ls ""${SRCFS}""/.snap/ | grep ""${EXT}"

# Determine how many snapshots have been created/mounted.
# UFS only support a maximum of 19 or 20.
numberOfSnapShots () {
	if [ ! -d "${SRCFS}" ]; then
		fatal "${SRCFS} is not a valid directory."
		exit 1
	fi
	
	echo $(ls "${SRCFS}"/.snap/ | grep "${EXT}" | wc -l)
}

# Decide whether it's necessary to remove some snapshots
# This is probably not the best implementation to deal with this algorithm, but the idea
# is: only remove backups if it's necessary, and only detach snapshots if it's necessary.

# Just remove backups if disk space is low on external drive.
# No unsnapping or unmounting needed, cuz the backup is not mounted. It's just sitting
# passively on the external drive.
COUNTER=0
while [ $(usage $(dirname "${EXTMEDIA}")) -gt "${MAXUSAGE}" ]; do
	COUNTER=$[ ${COUNTER} + 1 ];
	if [ ${COUNTER} -gt 19 ]; then break; fi # prevent endless loop.
	# Find out the name of the oldest snapshot
	debug "ls -t ${EXTMEDIA}/*.${EXT}.gz | tail -1"
	OLDESTBACKUP="$(ls -t ${EXTMEDIA}/*${EXT}.gz | tail -1)"
	
	if [ -z ${OLDESTBACKUP} ]; then
		fatal "Cannot determine oldest backup."
		exit
	fi

	echo "Usage is $(usage $(dirname ""${EXTMEDIA}""))% > ${MAXUSAGE}%. Removing oldest snapshot backup."
	debug rm "${OLDESTBACKUP}"
	rm "${OLDESTBACKUP}"
done

# Just detach snapshots if there are too many.
# 
COUNTER=0
while [ "$(numberOfSnapShots)" -ge "${MAXNUMBEROFSNAPSHOTS}" ]; do
	COUNTER=$[ ${COUNTER} + 1 ];
	if [ ${COUNTER} -gt 19 ]; then break; fi # prevent endless loop.
	# Find out the name of the oldest snapshot
	OLDESTSNAPSHOT="$(ls -dt ${SRCFS}/${SNAPSHOTSDIR}/*${EXT} | head -1)"
	# $OLDESTSNAPSHOT is a directory, and ls -dt returns the full path, so 
	# $OLDESTSNAPSHOT is a full absolute path.
	
	if [ -z ${OLDESTSNAPSHOT} ]; then
		fatal "Cannot determine oldest snapshot."
		exit
	fi

	echo "Too many mounted snapshots: $(numberOfSnapShots) >= ${MAXNUMBEROFSNAPSHOTS}. Detaching oldest snapshot."
	debug ${UNSNAP} "${OLDESTSNAPSHOT}"
	${UNSNAP} "${OLDESTSNAPSHOT}"
done

if  [ $(usage $(dirname "${EXTMEDIA}")) -gt ${MAXUSAGE} ] || [ $(numberOfSnapShots) -ge ${MAXNUMBEROFSNAPSHOTS} ]; then
	fatal "Despite attempts to clean up, Snaps or Backups are still above limit." "FATAL: Usage ${USAGE}% < ${MAXUSAGE}%,  $(numberOfSnapShots) < ${MAXNUMBEROFSNAPSHOTS}."
	exit
fi

echo "Usage is now $(usage $(dirname ""${EXTMEDIA}""))% < ${MAXUSAGE}%, or $(numberOfSnapShots) < ${MAXNUMBEROFSNAPSHOTS}. Proceeding."


echo "Take snapshot and mount it read only."
debug ${SNAP} ${SRCFS}
${SNAP} ${SRCFS}

# Find out the name of the latest snapshot
LATESTSNAPSHOT="$(ls ${SRCFS}/.snap/*${EXT} | tail -1)"

echo "Compress the latest snapshot and backup to external media."
debug echo "gzip -c ""${LATESTSNAPSHOT}"" > ""${EXTMEDIA}/${LATESTSNAPSHOT##*/}.gz"" "
gzip -c "${LATESTSNAPSHOT}" > "${EXTMEDIA}/${LATESTSNAPSHOT##*/}.gz"
