#!/bin/sh
#
#	$Id: rc-snap.sh,v 1.9 2009/04/04 07:51:23 cvs Exp $
#
# 

mount_backup () {
	base="${1}"

	if test ! -d "${base}/.snap"
	then
		return
	fi

	cd "${base}/.snap" || exit 72
	for date in *
	do
		if test ! -f "${base}/.snap/${date}"
		then
			continue
		fi
		#
		# alread mounted?
		if mount -t ufs | grep read-only | cut -d " " -f3 |
		fgrep -q "${base}/${SNAPSHOTSDIR}/${date}"
		then
			continue
		fi
		#
		# look for a free device
		for nr in `jot 40`
		do
			if test ! -c /dev/md${nr}
			then
				#
				# found a free device
				break
			fi
		done
		#
		# detach old device info (paranoid)
		# mdconfig -d -u "${nr}" 2>> /dev/null
		#
		# create device
		mdconfig -a -t vnode -o readonly -f "${base}/.snap/${date}" -u "${nr}"
		#
		# create a directory and mount the backup
		if test ! -d "${base}/backups/${date}"
		then
			mkdir "${base}/backups/${date}"
		fi
		mount -r -o noexec,nosuid /dev/md${nr} "${base}/backups/${date}"
	done
}
#
case "$1" in
start)
	echo -n " snapshots"
	for dir in `mount -t ufs | grep -v  read-only | cut -d " " -f3`
	do
		mount_backup "${dir}"
	done
	;;
stop)
	echo -n " snapshots"
	mount -t ufs | grep "^/dev/md.*backups/saved.*read-only" |
	while read dev on base mode
	do
		nr="${dev#/dev/md}"
		umount "${base}"
		mdconfig -d -u "${nr}"
	done
	;;
/*)
	mount_backup "${1}"
	;;
*)
	echo "Usage: ${0##*/} {start|stop|/path}" >&2
	exit 64
	;;
esac
#
# eof
