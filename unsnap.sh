#!/bin/bash
#
#
#
# 
#
# Unmount, detach and remove a snapshot permanently.
# 

# Location of config file 
CONFIG="$(dirname ${0})/snap.config"

if [ -f ${CONFIG} ]; then
        . ${CONFIG}
else
        echo "FATAL: cannot find config file ${CONFIG}"
        exit
fi

if [ ! $# -eq 1 ]; then
	echo "FATAL: incorrect number of arguments"
	echo
	echo "unsnap.sh <snapshotmountpoint>"
	echo
	echo " <snapshotmountpoint> must be without trailing slash!"
	echo
	exit
fi 

SNAPSHOTMOUNTPOINT=${1}
SNAPSHOTFILENAME=${SNAPSHOTMOUNTPOINT##*/}



if [ ! -d "${SNAPSHOTMOUNTPOINT}" ]; then
	echo "snapshot ${SNAPSHOTMOUNTPOINT}: dir does not exist" >&2
	exit 65
fi
#
# get the device in use
nr=`mount -t ufs | grep "^/dev/md.*${SNAPSHOTMOUNTPOINT}.*read-only" | while read dev on mbase mode
do
	echo "${dev#/dev/md}"
done`
#
if [ "${nr}" -le 0 -o "${nr}" -ge 40 ]; then
	echo "snapshot ${SNAPSHOTMOUNTPOINT}: is not a snapshot" >&2
	exit 65
fi
#
# umount the backup device
echo "Unmounting ${SNAPSHOTMOUNTPOINT}..."
umount "${SNAPSHOTMOUNTPOINT}"
RET=$?;
if [ $RET -eq 0 ]; then 
	#
	echo "Removing mount point"
	rmdir "${SNAPSHOTMOUNTPOINT}"
	RET=$?;
fi
if [ $RET -eq 0 ]; then 
	#
	# detach device
	echo "Detaching /dev/md${nr}..."
	mdconfig -d -u "${nr}"
	RET=$?;
fi
if [ $RET -eq 0 ]; then 
	#
	# destroy snapshort data
	echo "Destroying snapshot data... "
	rm -f "${SRCFS}/.snap/${SNAPSHOTFILENAME}"
fi
#
# eof
